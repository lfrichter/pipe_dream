#Pipe Dream 
@(Laravel)[php,laravel,webdev]
                                                                                               

#### Source
[![Composer](https://img.shields.io/badge/Composer-1.8.6-885630.svg?logo=composer&logoColor=white&style=for-the-badge)](https://getcomposer.org/)                                                                                                                                                                                                                                     [![git](https://img.shields.io/badge/Git-2.17.1-F05032.svg?logo=git&style=for-the-badge&logoColor=white)](https://git-scm.com/downloads)                                                                                                                                                                                                                                                                                                                                                                                                                                                                         [![php](https://img.shields.io/badge/php-7.2.19-777BB4.svg?logo=php&logoColor=white&style=for-the-badge)](http://php.net/)                                                                                                                                                                                                                                     [ ![Laravel](https://img.shields.io/badge/Laravel-5.8-E74430.svg?logo=laravel&logoColor=white&style=for-the-badge)](https://laravel.com/)                                                                                                                                                                                                                                     [![mySql](https://img.shields.io/badge/mysql-5.7.26-4479A1.svg?logo=mysql&logoColor=white&style=for-the-badge) ](https://www.mysql.com/)                                                                                                                                                                                                                                     [![nginx](https://img.shields.io/badge/nginx-1.14.0-269539.svg?logo=nginx&logoColor=white&style=for-the-badge)](http://nginx.org/)                                                                                                                                                                                                                                     

#### Package
[![PipeDream](https://img.shields.io/badge/PipeDream-0.0.75-E74430.svg?logo=laravel&style=for-the-badge&logoColor=white)](https://wiki.openstreetmap.org/wiki/Overpass_API/versions)                                                                                                                                                                                                                                     

#### Devlopment
[![npm](https://img.shields.io/badge/NPM-5.1.0-CB3837.svg?logo=npm&style=for-the-badge&logoColor=white)](https://www.npmjs.com)                                                                                                                                                                                                                                                                                                                                  [![Node.js](https://img.shields.io/badge/Node-12.4.0-339933.svg?logo=node.js&style=for-the-badge&logoColor=white)](https://nodejs.org/en/) 


## First build using Pipe Dream
- [Builder](http://localhost/pipe-dream)
```
Garage
location
capacity

Car
color
user_id

car_garage 

User 
name
email
email_verified_at
password
remember_token

password_resets
email
token
```